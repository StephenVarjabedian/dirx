package org.directoryx.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class HomeController {

    @RequestMapping("/")
    public ModelAndView dashboard() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("dashboard");
        mav.addObject("dashboardData", null);
        return mav;
    }
}
