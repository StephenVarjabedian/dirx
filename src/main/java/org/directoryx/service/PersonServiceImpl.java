package org.directoryx.service;

import org.directoryx.domain.Person;
import org.directoryx.repository.PersonRepository;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service("personService")
public class PersonServiceImpl extends GenericService<Person> implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Override
    public GraphRepository<Person> getRepository() {
        return personRepository;
    }

    public Person findByUserId(String userId) {
        return personRepository.findByUserId(userId);
    }

    public ArrayList<Person> getReportingStructure(String userId) {
        return personRepository.findReportingStructure(userId);
    }

    public ArrayList<Person> findByFirstName(String firstName) {
        return personRepository.findByFirstName(firstName);
    }

    public ArrayList<Person> getReportees(String firstName) {
        return personRepository.getReportees(firstName);
    }
}
