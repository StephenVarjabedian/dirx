package org.directoryx.repository;

import org.directoryx.domain.Person;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface PersonRepository extends GraphRepository<Person> {

    @Query("MATCH (n:Person ) WHERE n.userId={0} RETURN n;")
    Person findByUserId(String userId);

    @Query("MATCH (n:Person ) WHERE n.managerId={0} RETURN n;")
    Person findByLastName(String lastName);

    @Query("MATCH (n:Person ) WHERE n.firstName =~ {0} RETURN n;")
    ArrayList<Person> findByFirstName(String firstName);

    @Query("MATCH (n:Person) RETURN n;")
    ArrayList<Person> findAllPeople();

    @Query("MATCH (n:Person {firstName: {0}}) RETURN n;")
    ArrayList<Person> findByExactFirstName(String firstName);

    @Query("MATCH (n) DELETE n;")
    void deleteData();

    @Query("LOAD CSV WITH HEADERS FROM \"file:/code/directoryx/data/directoryx_sample2.csv\" AS line " +
            "CREATE (person:Person {personId: line.id, firstName: line.first_name, lastName: line.last_name, " +
            "userId: line.userid, title: line.title, status: line.status, photo: line.photo, managerId: line.managerId, " +
            "city: line.city, country: line.country, org: line.org, phone: line.phone, email: line.email});")
    void loadData();

    @Query("CREATE INDEX ON :Person(managerId);")
    void createManagerIdIndex();
    @Query("CREATE INDEX ON :Person(userId);")
    void createUserIdIndex();
    @Query("CREATE INDEX ON :Person(firstName);")
    void createFirstNameIndex();

    // @TODO make relationship without managerId
    @Query("MATCH (n:Person),(m:Person) WHERE n.userId=m.managerId CREATE (m)-[:WORKS_FOR]->(n);")
    void makeRelationships();

    @Query("MATCH (manager:Person { userId: {0} })<-[:WORKS_FOR]-(reports:Person) RETURN reports;")
    ArrayList<Person> getReportees(String userId);

    //way of searching for reporting structure of person
    @Query("MATCH (n:Person {userId: {0}})-[:WORKS_FOR*1..2]->(manager:Person)," +
            "(n:Person {userId: {0}})<-[:WORKS_FOR]-(reportee:Person) RETURN reportee, n, manager;")
    ArrayList<Person> findReportingStructure(String userId);

    @Query("MATCH (n:Person {userId: {0}})-[:WORKS_FOR*1..2]->(manager:Person)" +
            "MATCH (n:Person {userId: {0}})<-[:WORKS_FOR]-(reportee:Person) RETURN reportee;")
    ArrayList<Person> findReportingSnapshot(String userId);

}
